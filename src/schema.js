const { GraphQLSchema } = require('graphql')

const GraphQLRoot = require('./queries');
const GraphQLMutation = require('./mutations');

const schemaDefinition = {
    query: GraphQLRoot,
    mutation: GraphQLMutation,
}

module.exports = new GraphQLSchema(schemaDefinition);

// TODO: create admin panel
// TODO: should be able add, remove, update, change position too ??? ? ???
