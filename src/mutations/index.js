const {
    getApartment,
    getAparments,
    setApartment,
    removeApartment,
    updateApartment,
} = require('../database');
const {
    GraphQLID,
    GraphQLInt,
    GraphQLNonNull,
    GraphQLObjectType,
    GraphQLString,
} = require('graphql');
const {
    fromGlobalId,
    mutationWithClientMutationId,
    cursorForObjectInConnection,
} = require('graphql-relay');

const GraphQLAppartment = require('../types/Apartment');
const { edgeType: ApartmentEdgeType } = require('../types/ApartmentConnection');


const GraphQLAddApartmentMutation = mutationWithClientMutationId({
    name: 'AddApartment',
    inputFields: {
        title: { type: new GraphQLNonNull(GraphQLString) },
        slug: { type: GraphQLString },
        area: { type: GraphQLInt },
        rooms: { type: GraphQLInt },
        type: { type: GraphQLString },
        price: { type: GraphQLInt },
        fullAdress: { type: GraphQLString },
    },
    outputFields: {
        apartmentEdge: {
            type: ApartmentEdgeType,
            resolve({ apartment }) {
                const ap = getApartment(apartment.id);
                return {
                    cursor: cursorForObjectInConnection(getAparments(), ap),
                    node: ap,
                };
            }
        },
        apartment: { type: GraphQLAppartment },
    },
    mutateAndGetPayload (obj) {
        const apartment = setApartment(obj);
        return { apartment };
    },
});

const GraphQLUpdateApartmentMutation = mutationWithClientMutationId({
    name: 'UpdateApartment',
    inputFields: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        title: { type: GraphQLString },
    },
    outputFields: {
        apartment: { type: GraphQLAppartment }
    },
    mutateAndGetPayload({id, ...ap}) {
        const localId = Number(fromGlobalId(id).id);
        const apartment = updateApartment(localId, ap);
        return { apartment };
    },
});

const GraphQLRemoveApartmentMutation = mutationWithClientMutationId({
    name: 'RemoveApartment',
    inputFields: {
        id: { type: new GraphQLNonNull(GraphQLID) },
    },
    outputFields: {
        removedId: {
            type: GraphQLID,
            resolve: ({id}) => id,
        },
    },
    mutateAndGetPayload: async ({ id }) => {
        const localTodoId = +fromGlobalId(id).id;
        await removeApartment(localTodoId);
        return { id };
    },
});

const GraphQLMutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: {
        addApartment: GraphQLAddApartmentMutation,
        updateApartment: GraphQLUpdateApartmentMutation,
        removeApartment: GraphQLRemoveApartmentMutation,
    },
});

module.exports = GraphQLMutation;
