const {
    getAparments,
    getAparmentsLength,
    getApartment,
    setApartment,
    getApartmentBySlug,
    removeApartment,
    getUser,
} = require('../database');

const {
    GraphQLInt,
    GraphQLObjectType,
    GraphQLString,
} = require('graphql');
const {
    connectionArgs,
    connectionDefinitions,
    connectionFromArray,
} = require('graphql-relay');

const GraphQLAppartment = require('../types/Apartment');
const { connectionType: ApartmentConnection } = require('../types/ApartmentConnection');
const User = require('../types/User');
const { nodeField } = require('../types/NodeDefinitions');


const GraphQLRoot = new GraphQLObjectType({
    name: 'Root',
    fields: {
        apartments: {
            type: ApartmentConnection,
            args: {
                ...connectionArgs,
            },
            resolve: (obj, { ...args }) =>
                connectionFromArray(getAparments(), args),
        },
        apartment: {
            type: GraphQLAppartment,
            args: {
                slug: {
                    type: GraphQLString,
                }
            },
            resolve: (obj, { slug }) =>
                getApartmentBySlug(slug)
        },
        user: {
            type: User,
            resolve: () => getUser()
        },
        node: nodeField,
    },
});

module.exports = GraphQLRoot;
