// TODO: change it to list from immutable js
// TODO: add some DBHelper class with get/set apartments

const { List } = require('immutable');
const fs = require('fs');
const path = require('path');
let apartments = require('./appartments');
const slugify = require('@sindresorhus/slugify');

const getAparments = () => {
    return apartments
};

const getAparmentsLength = () => apartments.length

const getApartment = (id) => {
    return apartments.find(apartment => apartment.id === id)
};

const getApartmentBySlug = (slug) => new Promise(resolve => {
    const apartment = apartments.find(apartment => apartment.slug === slug);

    resolve(apartment)
});

const removeApartment = (id) => new Promise(resolve => {
    const aps = apartments.filter(ap => ap.id !== id);
    saveFile(aps);
    apartments = aps;
    resolve();
});

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
// TODO: generate proper id
const setApartment = ({id, title, area, rooms, type, price, fullAdress}) => {
    const appartment = {
        id: getRandomInt(0, 10000),
        title,
        slug: slugify(title),
        area,
        rooms,
        type,
        price,
        fullAdress
    };

    const list = [...apartments, appartment];
    saveFile(list);
    apartments = list;

    return appartment
};

const updateApartment = (id, apartment) => {
    const oldApartment = getApartment(id);
    const newApartment = Object.assign({}, oldApartment, apartment);
    newApartment.slug = slugify(newApartment.title);
    const updatedApartmentList = apartments.map(ap => {
        if(ap.id === id) {
            return newApartment
        } else {
            return ap
        }
    });

    saveFile(updatedApartmentList);

    return newApartment
};

function saveFile(list) {
    fs.writeFile(path.join('src', 'appartments.json'), JSON.stringify(list), (err) => {
        if(err) {
            console.log('error on create file');
            return;
        }

        console.log('File have been saved');
    })
}

const getUser = () => {
    return {};
};

module.exports = {
    getAparments,
    getAparmentsLength,
    getApartment,
    getApartmentBySlug,
    removeApartment,
    setApartment,
    updateApartment,
    getUser
};
