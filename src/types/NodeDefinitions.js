const {
    fromGlobalId,
    nodeDefinitions,
} = require('graphql-relay');

const { nodeInterface, nodeField } = nodeDefinitions(
    globalId => {
        const { type, id } = fromGlobalId(globalId);
        if (type === 'Apartment') {
            return getApartment(id);
        }
        return null;
    },
    obj => {
        if (obj.fullAdress) {
            return GraphQLAppartment;
        }
        return null;
    },
);

exports.nodeInterface = nodeInterface;
exports.nodeField = nodeField;
