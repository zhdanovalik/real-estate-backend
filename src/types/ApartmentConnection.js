const { GraphQLInt } = require('graphql');

const {
    connectionDefinitions,
} = require('graphql-relay');

const Apartment = require('./Apartment');

const { getAparmentsLength } = require('../database');

const {
    connectionType,
    edgeType,
} = connectionDefinitions({
    nodeType: Apartment,
    connectionFields: () => ({
        totalCount: {
            type: GraphQLInt,
            description: 'Total apartments length',
            resolve: getAparmentsLength,
        }
    }),
});

module.exports = {
    connectionType,
    edgeType,
};
