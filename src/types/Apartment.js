const {
    GraphQLInt,
    GraphQLObjectType,
    GraphQLString,
} = require('graphql');

const {
    globalIdField,
} = require('graphql-relay');

const { nodeInterface } = require('./NodeDefinitions');

const GraphQLAppartment = new GraphQLObjectType({
    name: 'Apartment',
    fields: {
        id: globalIdField(),
        title: { type: GraphQLString },
        slug: { type: GraphQLString },
        area: { type: GraphQLInt },
        rooms: { type: GraphQLInt },
        type: { type: GraphQLString },
        price: { type: GraphQLInt },
        fullAdress: { type: GraphQLString },
    },
    interfaces: [nodeInterface],
});

module.exports = GraphQLAppartment;
