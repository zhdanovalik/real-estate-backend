const {
    GraphQLObjectType,
} = require('graphql');
const {
    globalIdField,
    connectionArgs,
    connectionFromArray,
} = require('graphql-relay');
const { connectionType: ApartmentsConnection } = require('./ApartmentConnection');
const { nodeInterface } = require('./NodeDefinitions');
const { getAparments } = require('../database');

const User = new GraphQLObjectType({
    name: 'User',
    fields: {
        id: globalIdField('User'),
        apartments: {
            type: ApartmentsConnection,
            args: {
                ...connectionArgs
            },
            resolve: (obj, { ...args }) =>
                connectionFromArray(getAparments(), args),
        },
    },
    interfaces: [nodeInterface],
});

module.exports = User;
