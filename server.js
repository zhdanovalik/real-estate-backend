const express = require('express');
const routes = require('./routes');
const cors = require('cors');
const graphqlHTTP = require('express-graphql');
const schema = require('./src/schema');

const app = express()

app.use('/', routes)

app.use('/graphql', cors(), graphqlHTTP({
    schema,
    graphiql: true
}));

app.listen(7799, () => console.log('Example app listening on port 7799!'))
