const fs = require('fs');
const { printSchema } = require('graphql/utilities');
const path = require('path');

const schema = require('../data/schema');

fs.writeFileSync(
  path.join(__dirname, '../data/schema.graphql'),
  printSchema(schema),
)
